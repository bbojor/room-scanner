# Room mapper 

A semi-autonomous arduino robot communicating with a Python app via Bluetooth. The main algorithm is implemented in the Python application, which in turn sends appropriate commands to the robot in order to (hopefully) complete a top-down map of the area it is in. 

![Picture](./bot1.jpeg)

Screenshot of the desktop Python app with a scanned area.

![Picture](./scan.png)
