from math import cos,sin, radians
from enum import Enum 
from bluetooth_communication import *
from queue import PriorityQueue
from time import sleep
from itertools import chain

class Tiles(Enum):
    UNCHARTED = 1
    FREE = 2
    OCCUPIED = 3

SIGHT_RANGE = 75 # 2 tiles + offset
TILE_SIZE = 30 

class Actions(Enum):
    MOVE_FORWARD = 1
    TURN_LEFT = 2
    TURN_RIGHT = 3

class Node:

    def __init__(self, position, orientation, parent, action, cost):
        self.position = position
        self.parent = parent
        self.action = action
        self.cost = cost
        self.orientation = orientation

    def getPosition(self):
        return self.position

    def getOrientation(self):
        return self.orientation

    def getParent(self):
        return self.parent

    def getAction(self):
        return self.action

    def getCost(self):
        return self.cost


#draws the robot
def draw_bot(canvas, orientation, position):
    
    pos = (5.0/3 * position[0],  5.0/3 * position[1])
    if orientation == 0: #up
        canvas.create_line(pos[0], pos[1] - 10, pos[0] - 5, pos[1] + 10, fill = "red")
        canvas.create_line(pos[0], pos[1] - 10, pos[0] + 5, pos[1] + 10, fill = "red")
        canvas.create_line(pos[0] - 5, pos[1] + 10, pos[0] +  5, pos[1] + 10, fill = "red")

    elif orientation == 2:#down
        canvas.create_line(pos[0], pos[1] + 10, pos[0] - 5, pos[1] - 10, fill = "red")
        canvas.create_line(pos[0], pos[1] + 10, pos[0] + 5, pos[1] - 10, fill = "red")
        canvas.create_line(pos[0] - 5, pos[1] - 10, pos[0] +  5, pos[1] - 10, fill = "red")

    elif orientation == 3: #right
        canvas.create_line(pos[0] + 10, pos[1], pos[0] - 10, pos[1] - 5, fill = "red")
        canvas.create_line(pos[0] + 10, pos[1], pos[0] - 10, pos[1] + 5, fill = "red")
        canvas.create_line(pos[0] - 10, pos[1] + 5, pos[0] - 10, pos[1] - 5, fill = "red")
    
    elif orientation == 1: #left
        canvas.create_line(pos[0] - 10, pos[1], pos[0] + 10, pos[1] - 5, fill = "red")
        canvas.create_line(pos[0] - 10, pos[1], pos[0] + 10, pos[1] + 5, fill = "red")
        canvas.create_line(pos[0] + 10 , pos[1] - 5, pos[0] +  10, pos[1] + 5, fill = "red")

# helper function to draw the grid on a TKCanvas
def draw_grid(window, grid_map, orientation, position):
    #wipe canvas
    canvas  = window['Canvas'].TKCanvas
    canvas.delete('all')

    #draw grid lines and cells
    for i in range (1 ,10):
        canvas.create_line(0, i * 50, 500, i * 50)
        canvas.create_line(i * 50, 0, i * 50, 500)

    for i in range(0, 10):
        for j in range (0, 10):
            if grid_map[i][j] == Tiles.OCCUPIED: #space is untraversable
                canvas.create_rectangle(i * 50, j * 50, (i + 1) * 50, (j + 1) * 50, fill = 'blue')
            elif grid_map[i][j] == Tiles.UNCHARTED:
                canvas.create_rectangle(i * 50, j * 50, (i + 1) * 50, (j + 1) * 50, fill = 'gray')

    draw_bot(canvas, orientation, position)
    window.read(timeout = 0)


#checks if the position is valid inside the given map
def is_in_bounds(position, grid_map):
 if position[0] < 0 or position[1] < 0 or position[0] >= len(grid_map) or position[1] >= len(grid_map[0]):
     return False
 return True

# takes a position in centimeters and returns a pair of coordinates in grid space
def to_grid(position):
    res = list(position).copy()
    res[0] = int(res[0] // TILE_SIZE)
    res[1] = int(res[1] // TILE_SIZE)
    return res

# takes a position in the grid and returns the coordinates in cm
def to_cm(position):
    res = list(position).copy()
    res[0] = 30 * position[0] + 15;
    res[1] = 30 * position[1] + 15;
    return res

# clamps a pair of grid coordinates inside the bound of the given map
def clamp_to_grid(position, grid_map):
    res = list(position.copy())
    res[0] = max(0, min(res[0], len(grid_map)))
    res[1] = max(0, min(res[1], len(grid_map[0])))
    return res

# returns the furthest tile that is visible from the postion at the given angle
def get_position(position, orientation, angle, distance, debug):
   
    result = list(position).copy()
    angle = radians(angle)
    #compute the x/y offset based on the angle
    side_distance  = distance * cos(angle)
    front_distance = distance * sin(angle)
        
    if orientation == 0:
        result[0] += side_distance 
        result[1] -= front_distance
    elif orientation == 1:
        result[0] -= front_distance 
        result[1] -= side_distance
    elif orientation == 2:
        result[0] -= side_distance 
        result[1] += front_distance
    elif orientation == 3: 
        result[0] += front_distance 
        result[1] += side_distance

    if debug:
        print("front distance " + str(front_distance))
        print("side_distance " + str(side_distance))

    return result

#updates the grid_map acording to the sensor data
def update_grid_map(data_list,position, orientation, grid_map):

    side_distance = 0 
    front_distance = 0
    print("processing scan at position: " + str(position[0]) + " " + str(position[1]));
    for i in range(3):
        
        angle = i * 90

        if data_list[i] < 2: #measurement is out of valid value range, ignore it
            print("Error at angle " + str(angle))
            continue

        if data_list[i] <= SIGHT_RANGE : #detected value is within limits
        
            #compute the obstacle position based on the location of the bot
            grid_map_position = list([position[0],position[1]])
            
            grid_map_position = get_position(grid_map_position, orientation, angle, data_list[i], True)

            vision_cutoff = to_grid(grid_map_position)

            if grid_map_position[0] < position[0]:
                grid_map_position[0] -= 10
            else:
                grid_map_position[0] += 10

            if grid_map_position[1] < position[1]:
                grid_map_position[1] -= 10
            else:
                grid_map_position[1] += 10

            grid_map_position[0] = grid_map_position[0]
            grid_map_position[1] = grid_map_position[1]

            #update grid_map cell
            print("Obstacle detected at angle " + str(angle) + ", position " + str(grid_map_position) + " or  " + str(int(grid_map_position[0] // TILE_SIZE)) + "," + str(int(grid_map_position[1] // TILE_SIZE)))

            grid_map_position = to_grid(grid_map_position) 

            if not is_in_bounds(grid_map_position, grid_map):
                print("OUT OF BOUNDS")
                vision_cutoff = get_position(position, orientation, angle, SIGHT_RANGE, False) # i* 30 = angle in degrees
            else:
                grid_map[int(grid_map_position[0])][int(grid_map_position[1])] = Tiles.OCCUPIED #mark tile as occuppied

        else: #detected value is too far away
            print("Nothing detected at " + str(angle))
            vision_cutoff = get_position(position, orientation, angle, SIGHT_RANGE, True) # i* 30 = angle in degrees

        vision_cutoff = clamp_to_grid(to_grid(vision_cutoff), grid_map)
    
        #mark other visible cells as empty
        ray_pos = list([position[0],position[1]])

        angle = radians(angle)

        if orientation == 0:
            deltaX = cos(angle)
            deltaY = -sin(angle)
        elif orientation == 1:
            deltaX = -sin(angle)
            deltaY = -cos(angle)
        elif orientation == 2:
            deltaX = -cos(angle)
            deltaY = sin(angle)
        elif orientation == 3:
            deltaX = sin(angle)
            deltaY = cos(angle)
        
        grid_ray_pos = to_grid(ray_pos)
        while is_in_bounds(grid_ray_pos, grid_map) and (grid_ray_pos[0] != vision_cutoff[0] or grid_ray_pos[1] != vision_cutoff[1]):
            if grid_map[grid_ray_pos[0]][grid_ray_pos[1]] == Tiles.OCCUPIED:
                break;
            else:
                grid_map[grid_ray_pos[0]][grid_ray_pos[1]] = Tiles.FREE
                ray_pos[0] += deltaX
                ray_pos[1] += deltaY
                grid_ray_pos = to_grid(ray_pos)



#returns a list of valid neighbors with the moves needed to get to them and associated cost
def get_traversable_neighbors(position_node, grid_map):
    neighbors = []
    
    #try left
    orientation = position_node.getOrientation()
    new_pos = position_node.getPosition().copy()
    new_pos[0] -= 1 
    if is_in_bounds(new_pos, grid_map) and grid_map[new_pos[0]][new_pos[1]] == Tiles.FREE:
        if orientation == 0: #facing up
            neighbors += [Node(new_pos, 1, position_node, [Actions.TURN_LEFT,Actions.MOVE_FORWARD], position_node.getCost() + 2)]
        elif orientation == 1: # facing left
            neighbors += [Node(new_pos, 1,  position_node, [Actions.MOVE_FORWARD], position_node.getCost() + 1)]
        elif orientation == 2: # facing down
            neighbors += [Node(new_pos, 1, position_node, [Actions.TURN_RIGHT, Actions.MOVE_FORWARD],position_node.getCost() + 2)]
        elif orientation == 3: # facing right
            neighbors += [Node(new_pos, 1, position_node, [Actions.TURN_LEFT, Actions.TURN_LEFT, Actions.MOVE_FORWARD],position_node.getCost() + 3)]

    #try right
    new_pos = position_node.getPosition().copy()
    new_pos[0] += 1 
    if is_in_bounds(new_pos, grid_map) and grid_map[new_pos[0]][new_pos[1]] == Tiles.FREE:
        if orientation == 0: #facing up
            neighbors += [Node(new_pos, 3, position_node, [Actions.TURN_RIGHT,Actions.MOVE_FORWARD], position_node.getCost() + 2)]
        elif orientation == 1: # facing left
            neighbors += [Node(new_pos, 3, position_node, [Actions.TURN_LEFT, Actions.TURN_LEFT,Actions.MOVE_FORWARD], position_node.getCost() + 3)]
        elif orientation == 2: # facing down
            neighbors += [Node(new_pos, 3, position_node, [Actions.TURN_LEFT, Actions.MOVE_FORWARD], position_node.getCost() + 2)]
        elif orientation == 3: # facing right
            neighbors += [Node(new_pos, 3, position_node, [Actions.MOVE_FORWARD], position_node.getCost() + 1)]

    #try up
    new_pos = position_node.getPosition().copy()
    new_pos[1] -= 1 
    if is_in_bounds(new_pos, grid_map) and grid_map[new_pos[0]][new_pos[1]] == Tiles.FREE:
        if orientation == 0: #facing up
            neighbors += [Node(new_pos, 0, position_node, [Actions.MOVE_FORWARD], position_node.getCost() + 1)]
        elif orientation == 1: # facing left
            neighbors += [Node(new_pos, 0, position_node, [Actions.TURN_RIGHT, Actions.MOVE_FORWARD], position_node.getCost() + 2)]
        elif orientation == 2: # facing down
            neighbors += [Node(new_pos, 0, position_node, [Actions.TURN_LEFT ,Actions.TURN_LEFT, Actions.MOVE_FORWARD], position_node.getCost() + 3)]
        elif orientation == 3: # facing right
            neighbors += [Node(new_pos, 0, position_node, [Actions.TURN_LEFT, Actions.MOVE_FORWARD], position_node.getCost() + 2)]

    #try down
    new_pos = position_node.getPosition().copy()
    new_pos[1] += 1 
    if is_in_bounds(new_pos, grid_map) and grid_map[new_pos[0]][new_pos[1]] == Tiles.FREE:
        if orientation == 0: #facing up
            neighbors += [Node(new_pos, 2, position_node, [Actions.TURN_LEFT, Actions.TURN_LEFT, Actions.MOVE_FORWARD], position_node.getCost() + 3)]
        elif orientation == 1: # facing left
            neighbors += [Node(new_pos, 2, position_node, [Actions.TURN_LEFT, Actions.MOVE_FORWARD], position_node.getCost() + 2)]
        elif orientation == 2: # facing down
            neighbors += [Node(new_pos, 2, position_node, [Actions.MOVE_FORWARD], position_node.getCost() + 1)]
        elif orientation == 3: # facing right
            neighbors += [Node(new_pos, 2, position_node,[Actions.TURN_RIGHT, Actions.MOVE_FORWARD], position_node.getCost() + 2)]

    return neighbors

# returns true if any of nodes visible to the robot from the given position (1 position in front/to the left/to the right of the robot) are not explored yet
def visible_uncharted_neighbor(position, grid_map, orientation):

    if orientation == 0: # up
        if is_in_bounds([position[0] - 1, position[1]], grid_map) and grid_map[position[0] - 1][position[1]] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0] + 1, position[1]], grid_map) and grid_map[position[0] + 1][position[1]] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0], position[1] - 1], grid_map) and grid_map[position[0]][position[1] - 1] == Tiles.UNCHARTED:
            return True
        return False

    if orientation == 1: # left
        if is_in_bounds([position[0] - 1, position[1]], grid_map) and grid_map[position[0] - 1][position[1]] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0], position[1] + 1], grid_map) and grid_map[position[0]][position[1] + 1] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0], position[1] - 1], grid_map) and grid_map[position[0]][position[1] - 1] == Tiles.UNCHARTED:
            return True
        return False

    if orientation == 2: # down
        if is_in_bounds([position[0] - 1, position[1]], grid_map) and grid_map[position[0] - 1][position[1]] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0] + 1, position[1]], grid_map) and grid_map[position[0] + 1][position[1]] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0], position[1] + 1], grid_map) and grid_map[position[0]][position[1] + 1] == Tiles.UNCHARTED:
            return True
        return False


    if orientation == 3: # right
        if is_in_bounds([position[0] + 1, position[1]], grid_map) and grid_map[position[0] + 1][position[1]] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0], position[1] + 1], grid_map) and grid_map[position[0]][position[1] + 1] == Tiles.UNCHARTED:
            return True
        if is_in_bounds([position[0], position[1] - 1], grid_map) and grid_map[position[0]][position[1] - 1] == Tiles.UNCHARTED:
            return True
        return False

    return False

# returns the path to nearest unexplored tile to the current position, returns none if no unexplored tile is reachable 
def find_nearest_unexplored(grid_map, position, orientation):
    
    p_q = PriorityQueue()
    q_count = 0 # needed to make the  priority queue work for ties, simply gets increased everytime sth gets added to the queue
    current_node = Node(position, orientation, None, None, 0)
    p_q.put((current_node.getCost(), q_count, current_node))
    
    explored = []

    while not p_q.empty():
        
        _, _, current_node = p_q.get()
        explored += [current_node.getPosition()]
        print(current_node.getPosition())

        position = current_node.getPosition()
        orientation = current_node.getOrientation()

        #found an empty tile (goal)
        if visible_uncharted_neighbor(position, grid_map, orientation):
            print("Moving to " + str(current_node.getPosition()))
            solution = []
            while current_node.getParent():
                solution += [current_node.getAction()]
                current_node = current_node.getParent()
            solution.reverse()
            solution = list(chain.from_iterable(solution))
            print(solution)
            return solution

        for n in get_traversable_neighbors(current_node, grid_map):
            if n.getPosition() not in explored:
                q_count += 1
                p_q.put((n.getCost(), q_count, n))

   
# makes the bot travel along the given path (assumed to be valid)
def travel(path, connection, position, orientation, window, grid_map):
    
    new_pos = position.copy()
    while len(path) > 0 :
        action = path.pop(0)
        print(action)

        if action == Actions.MOVE_FORWARD:
            if orientation  == 0: #up
                new_pos[1] -= 1
            elif orientation == 1: # left
                new_pos[0] -= 1
            elif orientation == 2: #down
                new_pos[1] += 1
            elif orientation == 3: # right
                new_pos[0] += 1
            send_data("w", connection)
            data = receive_data(connection)
            if data == None or data.decode('ASCII') != 'w':
                raise Exception("Connection lost")


        elif action == Actions.TURN_LEFT:
            orientation = (orientation + 1) % 4
            send_data("a", connection)
            data = receive_data(connection)
            if data == None or data.decode('ASCII') != 'a':
                raise Exception("Connection lost")

        elif action == Actions.TURN_RIGHT:
            orientation = (orientation - 1) % 4
            send_data("d", connection)
            data = receive_data(connection)
            if data == None or data.decode('ASCII') != 'd':
                raise Exception("Connection lost")

        draw_grid(window, grid_map, orientation, to_cm(new_pos))
        sleep(3)

    return new_pos, orientation

# performs a full exploration of the area that is reachable by the bot from its starting position
def explore(start_position, grid_map, connection, orientation, window):
    

    position = to_grid(start_position)
    print("Starting exploration at position: " + str(position))
    if not is_in_bounds(position, grid_map):
        print("Original position of the robot is not valid for the given map")
        return 
    
    # mark starting tile as free
    grid_map[position[0]][position[1]] = Tiles.FREE

    draw_grid(window, grid_map, orientation, to_cm(position))

    path = []

    while path != None:
       
        #travel to new position
        position, orientation = travel(path, connection, position, orientation, window, grid_map)

        #scan at this position
        send_data("m", connection)
        data = receive_data(connection)
        if data == None or data.decode('ASCII') != 'm':
                raise Exception("Connection lost")

        sleep(6)
        data = receive_data(connection)
        if data != None: 
            data_list = data.split(b'|')
            data_list = [float(i) for i in data_list]
            print("Scan at position: " + str(position[0]) + " " + str(position[1]))
            print(data_list)
            update_grid_map(data_list, to_cm(position), orientation, grid_map)
            draw_grid(window, grid_map, orientation, to_cm(position))
        
        
        # find new position
        path = find_nearest_unexplored(grid_map, position, orientation)
      


