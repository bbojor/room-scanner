#include <MySoftwareSerial.h>
#include <Servo.h>
#include <avr/interrupt.h>
#include <NewPing.h>

// Use a SoftwareSerial for the bluetooth connection instead of the default RX/TX pins
// in order to leave the USB Serial open for debugging. Also solves an error which occurs
// attempting to program the board while a wire is connected to pin 0 (RX) (Caused by the
// fact that pins 0 and 1 are also used for the usb serial communication)

#define RX 4
#define TX 2

SoftwareSerial btSerial(TX, RX);

Servo sensorMotor;

//Motor pins
// Motor 1 = right, motor 2 = left
#define MOTOR1_PIN1 6
#define MOTOR1_PIN2 9
#define MOTOR2_PIN1 3
#define MOTOR2_PIN2 5
#define OK_SPEED 50
#define FORWARD_GAPS 70
#define TURN_GAPS 30
#define MOTOR_JUMPSTART 5
#define MOTOR_JUMPSTART_SPEED 100

//configuration for the ultrasonic distance sensor
#define TRIGGER_PIN 8
#define ECHO_PIN 7

#define SERVO_PIN 10

//analog pins used together with interrupts in order to record the wheel spin
#define LEFT_ENCODER A0
#define RIGHT_ENCODER 12

//count the number of gaps on the encoder wheels
volatile int leftGaps = 0, rightGaps = 0;

//speed at which motors should turn
volatile int leftSpeed = 0, rightSpeed = 0;

int servo_position = 0;

NewPing sensor(TRIGGER_PIN, ECHO_PIN, 5000);

void setup()
{

  //setup encoders attached to interrupts
  pinMode(LEFT_ENCODER, INPUT_PULLUP);
  pinMode(RIGHT_ENCODER, INPUT_PULLUP);

  cli();
  PCMSK1 |= B00000001; //activate interrupts on pin A0
  PCMSK0 |= B00010000; // atcivate interrupts on pin D12
  PCIFR   = B00000000; //clear interrupts
  PCICR  |= B00000011; //activate ports b (digital pins 8 to 13)
  //and c (analog pins 0 to 5)
  sei();

  //SoftwareSerials have trouble keeping up with higher frequencies
  //Since the bluetooth module used has a default frequency of 115200
  //it must be reconfigured to use a 9600 baud rate before becoming
  //usable, otherwise it will receive/transmit garbage data
  btSerial.begin(115200);
  delay(100);
  btSerial.print("$");
  btSerial.print("$");
  btSerial.print("$");
  delay(100);
  btSerial.println("U,9600,N");
  btSerial.begin(9600);

  // Setup motors
  pinMode(MOTOR1_PIN1, OUTPUT);
  pinMode(MOTOR1_PIN2, OUTPUT);
  pinMode(MOTOR2_PIN1, OUTPUT);
  pinMode(MOTOR2_PIN2, OUTPUT);
  go(0, 0);

  //setup servo
  sensorMotor.attach(SERVO_PIN);
  sensorMotor.write(servo_position);
  delay(2000);
  sensorMotor.detach();

 // Serial.begin(9600);
}

double measure()
{

  return sensor.convert_cm(sensor.ping_median(30));
}

void take_measurements()
{
  //START AT 0 DEEGREES
  double m = millis();

  double measured_distance;
  for (int i = 0; i < 3; i++)
  {
    servo_position = i * 90;

    sensorMotor.attach(SERVO_PIN);
    sensorMotor.write(servo_position);
    delay(800);
    sensorMotor.detach();

    measured_distance = measure();
    btSerial.print(measured_distance);
    if (servo_position != 180) //do not print separator after last value
      btSerial.print("|");

    delay(10);
  }

  //move back to position 0
  servo_position = 0;
  sensorMotor.attach(SERVO_PIN);
  sensorMotor.write(servo_position);
  delay(1200);
  sensorMotor.detach();
}

//Function for driving motors
//speeds take values between -255 and 255
void go(int speedLeft, int speedRight)
{
  if(speedLeft == 0 && speedRight == 0)
  {
    analogWrite(MOTOR2_PIN1, 0);
    analogWrite(MOTOR2_PIN2, 0);
    analogWrite(MOTOR1_PIN1, 0);
    analogWrite(MOTOR1_PIN2, 0);
  }
  if (speedLeft >= 0 && speedRight >= 0) //going forward
  {    
    analogWrite(MOTOR2_PIN2, 0);
    analogWrite(MOTOR1_PIN2, 0);
    
    //jump start the engines
    analogWrite(MOTOR2_PIN1, 2 * speedLeft);
    analogWrite(MOTOR1_PIN1, 2 * speedRight);
    delay(MOTOR_JUMPSTART);
    //then set the wanted movemet speed
    analogWrite(MOTOR2_PIN1, speedLeft);
    analogWrite(MOTOR1_PIN1, speedRight);
  }
  else if (speedLeft <= 0 && speedRight >= 0) //turn left
  {
    analogWrite(MOTOR2_PIN1, 0);
    analogWrite(MOTOR1_PIN2, 0);
  
    analogWrite(MOTOR2_PIN2, 2 * (- speedLeft));
    analogWrite(MOTOR1_PIN1, 2 * speedRight);
    delay(MOTOR_JUMPSTART);
    
    analogWrite(MOTOR2_PIN2, -speedLeft);
    analogWrite(MOTOR1_PIN1, speedRight);
  }
  else if (speedLeft > 0 && speedRight < 0) //turn right 
  {
    analogWrite(MOTOR2_PIN2, 0);
    analogWrite(MOTOR1_PIN1, 0);
    
    //jump start the engines
    analogWrite(MOTOR1_PIN2, 2 * (- speedRight));
    analogWrite(MOTOR2_PIN1, speedLeft);
    delay(MOTOR_JUMPSTART);
    //then set the wanted movement speed
    analogWrite(MOTOR1_PIN2, -speedRight);
    analogWrite(MOTOR2_PIN1, speedLeft);
  }

}

void loop()
{

  if (btSerial.available())
  {
    char value = (char)btSerial.read();

    btSerial.print(value);

    switch (value)
    {
      // go forward one square
      case 'w':  leftGaps = FORWARD_GAPS;
        rightGaps = FORWARD_GAPS;
        leftSpeed = OK_SPEED;
        rightSpeed = OK_SPEED;
      
        break;
      //turn left on the spot
      case 'a': leftGaps = TURN_GAPS;
        rightGaps = TURN_GAPS;
        leftSpeed = -OK_SPEED;
        rightSpeed = OK_SPEED;
      
        break;

      //turn right on the spot
      case 'd':  leftGaps = TURN_GAPS;
        rightGaps = TURN_GAPS;
        leftSpeed = OK_SPEED;
        rightSpeed = -OK_SPEED;
     
        break;

      case 'm': take_measurements();
        break;

      default: ;

    }
  }
 
  go(leftSpeed, rightSpeed);
}


ISR(PCINT1_vect)
{
  leftGaps--;
  if (leftGaps <= 0)
    leftSpeed = 0;
}

ISR(PCINT0_vect)
{
  rightGaps--;
  if (rightGaps <= 0)
    rightSpeed = 0;
}
