# This file contains the main loop of the application and the functions used to create the gui and related functionality

import PySimpleGUI as gui
import enum
import matplotlib.pyplot as pp
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from bluetooth_communication import *
from time import sleep
from map_utils import update_grid_map, explore, Tiles, draw_grid

nearby_devices = None
connected_device = None
connection = None

position = (45,  255)
orientation = 3

grid_map_size = 10
grid_map = [[Tiles.UNCHARTED for i in range(grid_map_size)] for i in range(grid_map_size)]

#redraws a window with the new specified layout (actually creates a new window on top of the old one before closing it in an attempt to make it a seamless as possible)
def redraw_window(old_window, new_layout, show_canvas = False):
     global position, orientation
     new_window = gui.Window(title = old_window.Title , layout = new_layout, location = window.current_location(), finalize = True)
     old_window.send_to_back()
     if show_canvas:
         draw_grid(new_window, grid_map, orientation, position)
     old_window.close() 
     return new_window


#clears the grid_map and sets bot back in it's original position
def reset(canvas):
    global grid_map, position, orientation
    grid_map =  [[Tiles.UNCHARTED for i in range(grid_map_size)] for i in range(grid_map_size)]
    position = (45,  255)
    orientation = 3
    canvas.delete('all')

# build the bluetooth connection menu
def create_connection_selection_menu(window, scan = False):    
    global nearby_devices

    if scan:
       nearby_devices = lookup_devices()
       
    connection_status_text = gui.Text("Status: not connected")

    if len(nearby_devices) > 0:
        combo_list = gui.Combo(nearby_devices, auto_size_text = True, pad = (10,10), key = "Device")

    else:
        combo_list = gui.Text("No nearby devices found")

    window_layout = [
            [gui.Canvas(key = "ConCanvas",size= (11, 11), background_color="#64778D"), connection_status_text],
            [combo_list, gui.Button("Connect"), gui.Button("Rescan")],
            [ gui.Canvas(key = "Canvas")]] 
    window = redraw_window(window, window_layout)
    circle = window['ConCanvas'].TKCanvas.create_oval(0, 0, 10 ,10, fill = "red")
    return window

def create_connected_menu(window):
    global connected_device
    connection_status_text = gui.Text("Status: connected")
    
    if connected_device[0] == None:
        connected_device[0] = "No name provided"

    window_layout = [
            [gui.Canvas(key = "ConCanvas",size = (11, 11), background_color = "#64778D"), connection_status_text],
            [gui.Text("Device: " + connected_device[0]),gui.Button("Disconnect")],
            [gui.Canvas(key = "Canvas", size=(500,500), background_color = "#FFFFFF")],
            [gui.Button("Forward")],
            [gui.Button("Left"), gui.Button("Right")],
            [gui.Button("Scan")],
            [gui.Checkbox(key = "move", text = "Manually position robot"), gui.Button("Explore autonomously")]]

    window = redraw_window(window, window_layout, show_canvas = True)
    circle = window['ConCanvas'].TKCanvas.create_oval(0, 0, 10 ,10, fill = "green")
    return window


########################################################### DRIVER CODE #####################################################################

#create gui
connection_status_text = gui.Text("Status: not connected")
window_layout = [[gui.Canvas(key = "ConCanvas", size = (11, 11), background_color = "#64778D" ), connection_status_text], [gui.Button("Scan for devices")], [gui.Canvas(key = "Canvas")]]
window = gui.Window(title = "Room Scanner", layout = window_layout, finalize = True)

circle = window['ConCanvas'].TKCanvas.create_oval(0, 0, 10 ,10, fill = "red")

# main loop
while True:

    event, values = window.read()

    if event == "Scan for devices" or event == "Rescan":
        window = create_connection_selection_menu(window, scan = True)
        
    elif event == "Connect":

        try:
            connected_device = window['Device'].Get()
        except: 
            connected_device = None
        connection  = connect(connected_device)
        if connection != None: 
            #connection succesfully established, go to connected menu
            window = create_connected_menu(window)

        else:
            #connection failed, display error and go back to connection selection menu
            gui.popup_error("Failed to establish connection with device" , keep_on_top = True, location = window.current_location())
            connected_device = None
            window = create_connection_selection_menu(window)

    elif event == "Disconnect":
        connected_device = None
        window = create_connection_selection_menu(window)
        connection.close()
        reset(window['Canvas'].TKCanvas)
        
    elif event == "Forward":
        pos = list(position)
        if orientation == 0:
            pos[1] -= 30
        elif orientation == 1:
            pos[0] -= 30
        elif orientation == 2:
            pos[1] += 30
        else:
            pos[0] += 30
        position = tuple(pos)
        if not window['move'].Get():
            send_data('w', connection)
            data = receive_data(connection)
            if data == None or data.decode('ASCII') != 'w':
                gui.popup_error("Connection with device lost" , keep_on_top = True, location = window.current_location())
                connected_device = None
                window = create_connection_selection_menu(window)
                connection.close()
                reset(window['Canvas'].TKCanvas)
        draw_grid(window, grid_map, orientation, position)

    elif event == "Left":
        orientation += 1
        if orientation > 3:
            orientation = 0
        if not window['move'].Get():
            send_data('a', connection)
            data = receive_data(connection)
            if data == None or data.decode('ASCII') != 'a':
                gui.popup_error("Connection with device lost" , keep_on_top = True, location = window.current_location())
                connected_device = None
                window = create_connection_selection_menu(window)
                connection.close()
                reset(window['Canvas'].TKCanvas)
        draw_grid(window, grid_map, orientation, position)

    elif event == "Right":
        orientation -= 1
        if orientation < 0:
            orientation = 3
        if not window['move'].Get():
            send_data('d', connection)
            data = receive_data(connection)
            if data == None or data.decode('ASCII') != 'd':
                gui.popup_error("Connection with device lost" , keep_on_top = True, location = window.current_location())
                connected_device = None
                window = create_connection_selection_menu(window)
                connection.close()
                reset(window['Canvas'].TKCanvas)
        draw_grid(window, grid_map, orientation, position)

    elif event == "Scan":
        print("Starting scan...")
        send_data("m", connection)
        data = receive_data(connection)
        if data == None or data.decode('ASCII') != 'm':
            gui.popup_error("Connection with device lost" , keep_on_top = True, location = window.current_location())
            connected_device = None
            window = create_connection_selection_menu(window)
            connection.close()
            reset(window['Canvas'].TKCanvas)
        else:
            sleep(6)
            data = receive_data(connection)
            print(data)
            if data != None: 
                data_list = data.split(b'|')

            if len(data_list) != 3: #data loss occured
                print("Data loss occured during transmission, dumping measurement")
            else:
                data_list = [float(i) for i in data_list]
                print(data_list)
                update_grid_map(data_list, position, orientation, grid_map)
                draw_grid(window, grid_map, orientation, position)

    elif event == "Explore autonomously":
        try:
            explore(list(position), grid_map, connection, orientation, window)
        except KeyboardInterrupt:
            gui.popup_error("Scan paused" , keep_on_top = True, location = window.current_location())
        except Exception:
            gui.popup_error("Connection with device lost" , keep_on_top = True, location = window.current_location())
            connected_device = None
            window = create_connection_selection_menu(window)
            connection.close()
            reset(window['Canvas'].TKCanvas)
        else:
            gui.popup_ok("Scan complete", keep_on_top = True, location = window.current_location())

    elif event == gui.WIN_CLOSED:        
        if connection != None:
            connection.close()
            
        break

window.close()

