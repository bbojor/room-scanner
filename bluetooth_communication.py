import bluetooth as bt

#searches for nearby devices and returns a list of names
#return a list of [device name,address] pairs
def lookup_devices():
    devices = bt.discover_devices()
    av_devices = []
    for d in devices:
        av_devices.append([bt.lookup_name(d), d])
    return av_devices

#connects to given device
def connect(device):
    
    if device == None:
        return None

    #create socket for sending data
    socket = bt.BluetoothSocket(bt.RFCOMM)
    port = 1
    try:
        socket.connect((device[1],port))
        socket.setblocking(False)
        socket.settimeout(100)
    except:
        socket = None
    
    return socket

#sends the given data to the socket object 
def send_data(data, connection):
    try:
        connection.send(data)
    except:
        pass

#receive some data from the socket object
def receive_data(connection):
    connection.settimeout(5.0)
    try:
        data = connection.recv(200)
    except:
        data = None
    return  data

